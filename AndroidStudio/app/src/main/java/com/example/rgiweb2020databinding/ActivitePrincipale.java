package com.example.rgiweb2020databinding;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;

import com.example.rgiweb2020databinding.databinding.LayoutActivitePrincipaleBinding;

public class ActivitePrincipale extends AppCompatActivity {

    private LayoutActivitePrincipaleBinding monLayoutActivitePrincipaleBinding;
    private Utilisateur leBelUtilisateur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setContentView(R.layout.layout_activite_principale);

         monLayoutActivitePrincipaleBinding =
                DataBindingUtil.setContentView(this, R.layout.layout_activite_principale);

        monLayoutActivitePrincipaleBinding.setLaBelleChaine("Toto");

        leBelUtilisateur = new Utilisateur("Leclercq","Yanis",71);
        monLayoutActivitePrincipaleBinding.setUnUtilisateur(leBelUtilisateur);
    }

    public void onClick_Modifier(View view) {
        monLayoutActivitePrincipaleBinding.setUnEntier(5);
        monLayoutActivitePrincipaleBinding.setLaBelleChaine("Titi");

        leBelUtilisateur.setNom("NouveauNom");
    }
}