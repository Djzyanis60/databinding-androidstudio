package com.example.rgiweb2020databinding;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class Utilisateur extends BaseObservable {

        private String nom;
        private String prenom;
        private int poids;
        public Utilisateur(String nom, String prenom, int poids) {
            this.nom = nom;
            this.prenom = prenom;
            this.poids = poids;
        }
        @Override
        public String toString() {
            return "utilisateur{" +
                    "nom='" + nom + '\'' +
                    ", prenom='" + prenom + '\'' +
                    ", poids=" + poids +
                    '}';
        }

        public Utilisateur(){
            this("nom par defaut","prenom par defaut",0);
        }
        @Bindable
        public String getNom() {
            return nom;
        }
        public void setNom(String nom) {

            this.nom = nom;
            notifyPropertyChanged(BR.nom);
        }
        @Bindable
        public String getPrenom() {
            return prenom;
        }
        public void setPrenom(String prenom) {

            this.prenom = prenom;
            notifyPropertyChanged(BR.prenom);
        }
        @Bindable
        public int getPoids() {
            return poids;
        }
        public void setPoids(int poids) {

            this.poids = poids;
            notifyPropertyChanged(BR.poids);
        }
}
